package com.example.jacobl_book_nerd.model.response

import com.example.jacobl_book_nerd.model.local.entity.Book


class BookDTO (
    val book : List<Books>

){
    data class Books(
        val title: String,
    )
}