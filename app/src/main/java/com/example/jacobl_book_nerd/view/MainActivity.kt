package com.example.jacobl_book_nerd.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.jacobl_book_nerd.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)