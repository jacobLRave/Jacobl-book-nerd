package com.example.jacobl_book_nerd.view

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BookApplication :Application(){
}