package com.example.jacobl_book_nerd.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.example.jacobl_book_nerd.model.BookRepo
import com.example.jacobl_book_nerd.model.local.entity.Book
import com.example.jacobl_book_nerd.model.response.BookDTO
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class BookViewModel @Inject constructor(private val repo: BookRepo)  : ViewModel() {


    val state: LiveData<BookState> = liveData {
        emit(BookState(isLoading = true))
        val book = repo.getBooks()
        emit(BookState(books = book))
    }

    data class BookState(
        val isLoading: Boolean = false,
        val books: List<Book> = emptyList()
    )
}