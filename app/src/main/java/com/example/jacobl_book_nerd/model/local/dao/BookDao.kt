package com.example.jacobl_book_nerd.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.jacobl_book_nerd.model.local.entity.Book
import com.example.jacobl_book_nerd.model.response.BookDTO

@Dao
interface BookDao {

    @Query("SELECT * FROM book")
    suspend fun getAll(): List<Book>

    @Insert
    suspend fun insert(book: List<Book>)
}
