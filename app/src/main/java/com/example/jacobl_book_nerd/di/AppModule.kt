package com.example.book.di

import com.example.jacobl_book_nerd.model.remote.BookService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun BookService() :  BookService = BookService.getInstance()


}