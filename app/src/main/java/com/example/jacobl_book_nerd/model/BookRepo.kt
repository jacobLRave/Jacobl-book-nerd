package com.example.jacobl_book_nerd.model

import android.content.Context
import android.util.Log
import com.example.jacobl_book_nerd.model.local.BookDatabase
import com.example.jacobl_book_nerd.model.local.entity.Book
import com.example.jacobl_book_nerd.model.remote.BookService
import com.example.jacobl_book_nerd.model.response.BookDTO
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

private const val TAG = "BookRepo"

class BookRepo @Inject constructor(
    val bookService: BookService,
    @ApplicationContext context: Context
) {

    val bookDao = BookDatabase.getInstance(context).bookDao()
    suspend fun getBooks(): List<Book> = withContext(Dispatchers.IO) {
        val cachedbooks: List<Book> = bookDao.getAll()


        return@withContext cachedbooks.ifEmpty {
            val bookUrls: BookDTO = bookService.getBooks()
            val books: List<Book> = bookUrls.book.map{ Book(title = it.title)}
            bookDao.insert(books)
            Log.d("state", books.toString())

            return@ifEmpty books
        }
    }
}