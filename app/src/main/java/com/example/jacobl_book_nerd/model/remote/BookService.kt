package com.example.jacobl_book_nerd.model.remote

import com.example.jacobl_book_nerd.model.local.entity.Book
import com.example.jacobl_book_nerd.model.response.BookDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface BookService {

    companion object{
        const val BASE_URL ="https://the-dune-api.herokuapp.com"

        fun getInstance(): BookService {
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(BookService::class.java)
        }
    }

    @GET("/books/100")
    suspend fun getBooks() : BookDTO


}
//https://the-dune-api.herokuapp.com/books/100